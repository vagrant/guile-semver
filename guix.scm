;;; Guile-SemVer
;;; Copyright 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Guile-SemVer.
;;;
;;; Guile-SemVer is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-SemVer is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-SemVer.  If not, see <https://www.gnu.org/licenses/>.

(use-modules ((gnu packages autotools) #:select (autoconf automake))
             ((gnu packages guile) #:select (guile-3.0))
             ((gnu packages pkg-config) #:select (pkg-config))
             (guix build-system gnu)
             (guix download)
             ((guix licenses) #:prefix license:)
             (guix packages))

(package
  (name "guile-semver")
  (version "0.1.1")
  (source #f)
  (build-system gnu-build-system)
  (native-inputs
   `(("autoconf" ,autoconf)
     ("automake" ,automake)
     ("pkg-config" ,pkg-config)))
  (inputs
   `(("guile" ,guile-3.0)))
  (home-page "https://ngyro.com/software/guile-semver.html")
  (synopsis "Semantic Versioning (SemVer) for Guile")
  (description "This Guile library provides tools for reading,
comparing, and writing Semantic Versions.  It also includes ranges in
the style of the Node Package Manager (NPM).")
  (license license:gpl3+))
